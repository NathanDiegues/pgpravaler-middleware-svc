Solução para CORS

minikube --extra-config apiserver.cors-allowed-origins=["http://*"] start

Fixed by adding --cors-allowed-origins=["http://*"] argument to /etc/default/kube-apiserver file. Then restarted to kube-apiserver.

https://stackoverflow.com/questions/50352621/where-is-kube-apiserver-located