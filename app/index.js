// ./index.js
const express = require('express');
const router = require('express-promise-router')();
const request = require('request-promise');
const session = require('cookie-session'); //deixar sessão segura
const helmet = require('helmet'); //medidas de segurança
const cors = require('cors'); //CORS

const app = express();
const port = 8080;

const user_svc = "http://user-service.default.svc.cluster.local";
const boleto_svc = "http://boleto-service.default.svc.cluster.local";

var expireDate = 60 * 120 * 1000; //2 horas
app.use(session({
    name: 'pagPravaler',
    keys: ['c3rHwJuThcJkvct6', 'SMxSXLwy3DhTw4RE'],
    maxAge: expireDate
}));
app.use(helmet());
app.use(cors()); //configurar depois para apenas aceitar da url de prod e qa

//chamar apis pelo nome interno do minikube da problema de cors, deve-se chamar esta
//api pelo ip mesmo

//para processar formulários
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.send('Microservice Middleware OK');
});

router.get('/user', async (req, res) => {
    request.get(user_svc)
        .then((data) => {
            res.send(JSON.stringify({ "message": data }));
        }).catch(err => {
            res.status(500).send(JSON.stringify({ "message": "Microservice user offline" }));
        })
});

router.get('/boleto', async (req, res) => {
    request.get(boleto_svc)
        .then((data) => {
            res.send(JSON.stringify({ "message": data }));
        }).catch(err => {
            res.status(500).send(JSON.stringify({ "message": "Microservice boleto offline" }));
        })
});

//Função default para definir requests
function defineRequest(uri, data) {
    return {
        uri: uri,
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: data,
        json: true
    }
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

//Função inicial do registro do usuário
router.post('/register', async (req, res) => {

    //variável de resposta
    var resp = new Object();

    var addUser = defineRequest(user_svc+'/user/register', {});

    //cria o usuário
    const req1 = await request.post(addUser)
        .then((data) => {
            resp.usuario = "Usuário criado com sucesso";
            return data
        }).catch(err => {
            res.status(500).send(JSON.stringify({ "error": err }));
        })

    var addExtraInfo = defineRequest(user_svc + '/user/addInfo', { "id": await req1 });

    //adiciona as informações extras
    const req2 = await request.post(addExtraInfo)
        .then((data) => {
            // res.send("Usuário e suas informações criadas com sucesso, id = "+req1)
            return "Dados extras criados com sucesso";
        }).catch(err => {
            res.status(500).send(JSON.stringify({ "error": err }));
        })
    
    resp.informacao = req2;

    //aguarda o id do usuário para criar proposta
    req.body.usr_id = await req1;
    var addProposta = defineRequest(boleto_svc + '/proposta/register', req.body);

    //cria proposta do usuário e já adiciona informações iniciais
    const req3 = await request.post(addProposta)
        .then((data) => {
            // res.status(200).send("Proposta criada com sucesso")
            resp.proposta = "Proposta criada com sucesso.";
            return data;
        }).catch(err => {
            res.status(500).send(JSON.stringify({ "error": err }));
        })

    for(var i = 0; i < req.body.parcelas; i++) {
        //calcula número de parcelas
        var vencimento = new Date(req.body.vencimento);
        vencimento.setMonth(vencimento.getMonth()+i);
        vencimento = formatDate(vencimento);

        var valor = req.body.boleto_valor/req.body.parcelas;

        if(i === 0) valor += 50;

        //todo fazer calculo do valor dos boletos no backend
        //"valor":        req.body.valor[i],
        var bolet = {
            "usr_id":       req1,
            "pro_id":       req3,
            "valor":        valor,
            "vencimento":   vencimento
        };

        var addBoleto = defineRequest(boleto_svc+'/boleto/register', bolet);
        var reqBoleto = await request.post(addBoleto)
            .then((data) => {
                return (i+1)+" Boletos criado com sucesso";
            }).catch(err => {
                res.status(500).send(JSON.stringify({ "error": err }));
            })
        //melhorar essa resposta
        resp.boleto = reqBoleto;
    }

    res.send(JSON.stringify(resp));
});

router.post('/update', async (req, res) => {

    // res.header("Access-Control-Allow-Origin", "*");
    // res.header("Access-Control-Allow-Headers", "Origin, X-Request-Width, Content-Type, Accept");

    req.body.usr_id = req.body.id;

    //define caminhos de alteração
    const updateUser = defineRequest(user_svc + '/user/update', req.body)
    const updateInfo = defineRequest(user_svc + '/user/updateInfo', req.body)
    const updateProposta = defineRequest(boleto_svc + '/proposta/update', req.body)

    //variável de resposta
    var resp = new Object();

    const req1 = await request.post(updateUser)
        .then((data) => {
            return data;
        }).catch(err => {
            res.status(500).send(JSON.stringify({ "error": err }));
        })

    const req2 = await request.post(updateInfo)
        .then((data) => {
            return data;
        }).catch(err => {
            res.status(500).send(JSON.stringify({ "error": err }));
        })

    const req3 = await request.post(updateProposta)
        .then((data) => {
            return data;
        }).catch(err => {
            res.status(500).send(JSON.stringify({ "error": err }));
        })

    //coloca as respostas
    resp.user = req1;
    resp.info = req2;
    resp.proposta = req3;

    res.status(200).send(JSON.stringify(resp));
})

app.use('/', router);

app.listen(port, () => console.log(`Middleware listening on port ${port}`));